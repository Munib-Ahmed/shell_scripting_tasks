cd	#Home Directory

check_dir="Backup"	#Backup Directory
if  [ -d "$check_dir" ]
then 
	echo "The Directory already Exist"
else 
	echo "The Directory doesnot Exist"
	mkdir $check_dir
fi

cd $1	#Specified Directory

for i in $(find . -name '*.c'| sed 's|^./||');
do
        echo "File_name = " $i
	if [ -f /home/emumba/$check_dir/$i ]
	then
		if cmp -s "/home/emumba/$check_dir/$i" "/home/emumba/$1/$i" ;
		then
			echo "Status = existed nothing changed"
			echo " "
		else 
			echo "Status = the file has been updated"
			echo " "
			cp /home/emumba/$1/$i /home/emumba/$check_dir/$i
		fi 
	else
		echo "file had no previous copy and is now backed up"
		cp /home/emumba/$1/$i /home/emumba/$check_dir/$i
	fi
#	if (find /home/emumba/$check_dir -type f -name $i)
done 
