#!/bin/bash

touch task_6_number_data.txt
touch task_6_sorted_number_data.txt
#touch results.txt

awk '{print $4}' raw_data_for_stats_calculation.txt > task_6_number_data.txt
cat task_6_number_data.txt | sort -n >task_6_sorted_number_data.txt

#echo "" > results.txt

awk 'NR==1{print "minimum = " $1}' task_6_sorted_number_data.txt
#awk 'NR==1{print $1}' task_6_sorted_number_data.txt >> results.txt

awk 'NR==200{print "maximum = " $1}' task_6_sorted_number_data.txt
#awk 'NR==200{print $1}' task_6_sorted_number_data.txt >> results.txt
#awk '{s+=1}END{print $($s)}' task_6_sorted_number_data.txt

awk -v sum=0 '{sum=sum+$0}END{print "Sum = " sum}' task_6_sorted_number_data.txt
#awk -v sum=0 '{sum=sum+$0}END{print sum}' task_6_sorted_number_data.txt >> results.txt

awk -v sum=0, -v inc=0 '{sum=sum+$0;inc+=1}END{print "Mean = " sum/inc}' task_6_sorted_number_data.txt
#awk -v sum=0, -v inc=0 '{sum=sum+$0;inc+=1}END{print sum/inc}' task_6_sorted_number_data.txt >> results.txt

awk 'NR==100,NR==101{sum=sum+$0}END{print "Median = "sum/2}' task_6_sorted_number_data.txt
#awk 'NR==100,NR==101{sum=sum+$0}END{print sum/2}' task_6_sorted_number_data.txt >> results.txt
